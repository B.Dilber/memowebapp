﻿using Microsoft.AspNetCore.Hosting;

[assembly: HostingStartup(typeof(MemoWebApp.Areas.Identity.IdentityHostingStartup))]
namespace MemoWebApp.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) =>
            {
            });
        }
    }
}